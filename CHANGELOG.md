## Doing
* Make ListItem and ScaffoldWithImage more customizable for poll page
* Add overlay_support loading widget
* Add `isLink` to `ListItem`
* Change `TextInput` padding
* Enable `TextInputType` for `TextInput` widget

## [0.1.2]
* Add colorSecondaryDescription and TIP NavButton style
## [0.1.1]
* Add navButton widget
* Add textInput widget
* Fix HTMLSummary Bug

## [0.1.0] First commit

* Extract the common code from `client-desktop`
* Provide the color and size constants
* Provide the common widgets
