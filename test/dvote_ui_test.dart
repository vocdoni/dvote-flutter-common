import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

import 'package:dvote_common/dvote_common.dart';

void main() {
  testExampleWidget();
}

void testExampleWidget() {
  testWidgets('EventuallBuilder handles one value',
      (WidgetTester tester) async {
    await tester.pumpWidget(MyWidgetTester(null));

    expect(find.text("Test"), findsOneWidget,
        reason: "value should be null, but it isn't");

    // name.value = "Hello";
    // await tester.pump();
    await tester.pumpWidget(MyWidgetTester("Hello"));

    expect(find.text("Hello"), findsOneWidget,
        reason: "value should be Hello, but it isn't");
  });
}

class MyWidgetTester extends StatelessWidget {
  final String text;
  const MyWidgetTester(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DVote UI Test',
      home: Scaffold(
        body: Text(text ?? "Test"),
      ),
    );
  }
}
