# DVote Common

This Flutter package contains the common elements used by client UI projects.

The following components are currently abstracted:
- Constants
  - Colors
  - Sizes
- UI Widgets

### TO DO

- [ ] Data Models
- [ ] Data Persistence

### NOTES

- In order to use the system haptics in `drawPattern.dart` for Android, the app's `AndroidManifest.xml` file must include `<uses-permission android:name="android.permission.VIBRATE"/>`
