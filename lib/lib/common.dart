import 'dart:convert';
import 'dart:typed_data';

String processIpfsImageUrl(String uri,
    {String ipfsDomain = "https://ipfs.io/ipfs/"}) {
  return uri.replaceFirst("ipfs://", ipfsDomain);
}

String trimInlineImagePrefix(String uri) {
  if (!uri.startsWith("data:image/")) return uri;

  final rightPivot = uri.indexOf(";base64,");
  final type = uri.substring(11, rightPivot);

  switch (type) {
    case "png":
    case "jpeg":
    case "jpg":
    case "gif":
    case "webp":
    case "webm":
      return uri.substring(rightPivot + 8);
    default:
      return uri;
  }
}

Uint8List parseInlineImageUri(String uri) {
  final b64Content = trimInlineImagePrefix(uri);
  return base64Decode(b64Content);
}
