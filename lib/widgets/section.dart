import "package:flutter/material.dart";
import 'package:dvote_common/constants/colors.dart';

class Section extends StatelessWidget {
  final String text;
  final bool withDectoration;

  Section({this.text, this.withDectoration = true});

  @override
  Widget build(context) {
    Color decorationColor =
        withDectoration ? colorLightGuide : Colors.transparent;

    return Container(
        padding: EdgeInsets.fromLTRB(paddingPage, 24, paddingPage, 16),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(height: 1, color: decorationColor),
              ),
              buildText(text),
              Expanded(
                child: Container(height: 1, color: decorationColor),
              ),
            ]));
  }

  Widget buildText(String text) {
    if (text == null) return Container();
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 24),
        child: Text(text,
            style: new TextStyle(
                fontSize: 16, color: colorGuide, fontWeight: FontWeight.w400)));
  }
}
