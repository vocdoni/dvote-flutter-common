import "package:flutter/material.dart";
import 'package:dvote_common/constants/colors.dart';

class DashboardRow extends StatelessWidget {
  final List<Widget> children;

  DashboardRow({this.children});

  @override
  Widget build(context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          paddingPage, spaceElement, paddingPage, spaceElement),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children),
    );
  }
}
