import 'package:dvote_common/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextInput extends StatelessWidget {
  final String hintText;
  final void Function(String) onSubmitted;
  final void Function(String) onChanged;
  final TextCapitalization textCapitalization;
  final TextInputFormatter inputFormatter;
  final TextInputType keyboardType;
  final bool enabled;

  const TextInput(
      {this.hintText,
      this.onSubmitted,
      this.onChanged,
      this.textCapitalization = TextCapitalization.sentences,
      this.inputFormatter,
      this.keyboardType = TextInputType.emailAddress,
      this.enabled = true});

  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: enabled,
      keyboardType: keyboardType,
      inputFormatters: [
        inputFormatter == null
            ? FilteringTextInputFormatter.deny('\n')
            : inputFormatter
      ],
      autocorrect: false,
      autofocus: false,
      textCapitalization: textCapitalization,
      style: TextStyle(
        fontWeight: fontWeightLight,
        color: colorDescription,
        fontSize: 17,
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
        filled: true,
        border: _emptyTextInputBorder(),
        focusedBorder: _emptyTextInputBorder(),
        enabledBorder: _emptyTextInputBorder(),
        disabledBorder: _emptyTextInputBorder(),
        errorBorder: _emptyTextInputBorder(),
        hintText: hintText,
      ),
      onSubmitted: onSubmitted,
      onChanged: onChanged,
    );
  }
}

OutlineInputBorder _emptyTextInputBorder() {
  return OutlineInputBorder(
    borderSide: BorderSide(color: colorBaseBackground),
    borderRadius: BorderRadius.circular(10),
  );
}
