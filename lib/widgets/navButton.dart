import 'package:dvote_common/constants/colors.dart';
import 'package:flutter/material.dart';

enum NavButtonStyle {
  NEXT_FILLED,
  NEXT,
  BASIC,
  TIP,
}

class NavButton extends StatelessWidget {
  final String text;
  final void Function() onTap;
  final bool isDisabled;
  final NavButtonStyle style;

  const NavButton(
      {this.text,
      this.onTap,
      this.isDisabled = false,
      this.style = NavButtonStyle.BASIC});

  @override
  Widget build(BuildContext context) {
    if (text == null) return Container();
    if (style == NavButtonStyle.BASIC)
      return FlatButton(
        padding: EdgeInsets.zero,
        onPressed: isDisabled ? null : onTap,
        child: Text(
          text,
          style: TextStyle(
            fontWeight: fontWeightSemiBold,
            fontSize: fontSizeBase,
            color: colorSecondaryDescription,
          ),
        ),
      );
    if (style == NavButtonStyle.TIP)
      return FlatButton(
        padding: EdgeInsets.zero,
        onPressed: isDisabled ? null : onTap,
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
              child: Icon(
                Icons.info_outline,
                color: colorSecondaryDescription,
              ),
            ),
            Text(
              text,
              style: TextStyle(
                fontWeight: fontWeightSemiBold,
                fontSize: fontSizeBase,
                color: colorSecondaryDescription,
              ),
            ),
          ],
        ),
      );
    if (style == NavButtonStyle.NEXT)
      return FlatButton(
        onPressed: isDisabled ? null : onTap,
        textColor: colorBlue,
        padding: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 14,
        ),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
              child: Icon(
                Icons.arrow_forward_sharp,
              ),
            ),
            Text(
              text,
              style: TextStyle(
                fontWeight: fontWeightSemiBold,
                fontSize: fontSizeBase,
              ),
            ),
          ],
        ),
      );
    if (style == NavButtonStyle.NEXT_FILLED)
      return ElevatedButton(
        onPressed: isDisabled ? null : onTap,
        style: ButtonStyle(
          backgroundColor: isDisabled
              ? MaterialStateProperty.all<Color>(colorDescriptionPale)
              : MaterialStateProperty.all<Color>(colorBlue),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26),
          )),
          padding: MaterialStateProperty.all(EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 14,
          )),
          elevation: MaterialStateProperty.all(1),
        ),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
              child: Icon(
                Icons.arrow_forward_sharp,
                color: colorCardBackround,
              ),
            ),
            Text(
              text,
              style: TextStyle(
                fontWeight: fontWeightSemiBold,
                fontSize: fontSizeBase,
                color: colorCardBackround,
              ),
            ),
          ],
        ),
      );
    return Container();
  }
}
