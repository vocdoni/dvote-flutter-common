import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:dvote_common/constants/colors.dart';
import 'package:flutter/material.dart';

import '../navButton.dart';

void _noHaptic() {}

class EnterPin extends StatefulWidget {
  final int totalDigits;
  final String continueText;
  final Color digitColor;
  final Color hitColor;
  final EdgeInsets padding;
  final double indicatorSpace;
  final void Function() onPinHaptic;
  final void Function(BuildContext context, List<int> pin) onPinStopped;

  /// if `continueText` is null, `onPinStopped` triggers when `totalDigits` is met. Otherwise user must press continue button
  EnterPin(
      {Key key,
      this.digitColor = Colors.grey,
      this.continueText,
      this.totalDigits = 4,
      this.padding,
      this.indicatorSpace = spaceCard * 2,
      this.hitColor = colorBluePale,
      this.onPinHaptic = _noHaptic,
      this.onPinStopped})
      : super(key: key);

  @override
  _EnterPinState createState() => _EnterPinState();
}

class _EnterPinState extends State<EnterPin> {
  List<int> pin = <int>[];

  @override
  void initState() {
    pin = <int>[];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final hasContinueButton = widget.continueText != null;
    return Padding(
      padding:
          widget.padding ?? EdgeInsets.symmetric(horizontal: paddingPage * 2),
      child: Column(children: [
        _buildValidEntries(),
        Padding(
          padding: EdgeInsets.only(top: widget.indicatorSpace),
          child: GridView.count(
            crossAxisCount: 3,
            childAspectRatio: 1.4,
            shrinkWrap: true,
            mainAxisSpacing: 0,
            crossAxisSpacing: 0,
            children: _renderButtons(pin.length >= widget.totalDigits),
          ),
        ),
        SizedBox(
          height: 40,
          child: hasContinueButton
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Spacer(),
                      NavButton(
                          isDisabled: pin.length != widget.totalDigits,
                          style: NavButtonStyle.NEXT,
                          text: widget.continueText ?? "Continue",
                          onTap: () => widget.onPinStopped(context, pin)),
                    ],
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(),
                ),
        ),
      ]),
    );
  }

  Widget _buildValidEntries() {
    return CustomPaint(
        painter: Sketcher(total: widget.totalDigits, filled: pin.length));
  }

  List<Widget> _renderButtons(bool disabled) {
    List<Widget> buttons = List<Widget>();
    buttons.addAll(<int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 0].map((int val) {
      return Center(
        child: TextButton(
          child: SizedBox(
            width: 60,
            height: 70,
            child: Text(
              val.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: colorGuide,
                  fontSize: fontSizeDigit,
                  fontWeight: fontWeightLight,
                  fontFamily: "Open Sans"),
            ),
          ),
          style: TextButton.styleFrom(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            onSurface: Colors.transparent,
            // padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(65, 90)),
            ),
            animationDuration: Duration(milliseconds: 1),
            primary: widget.hitColor,
          ),
          onPressed: disabled
              ? null
              : () {
                  widget.onPinHaptic();
                  setState(() {
                    if (pin.length < widget.totalDigits) pin.add(val);
                  });
                  if (pin.length >= widget.totalDigits &&
                      widget.continueText == null)
                    widget.onPinStopped(context, pin);
                },
        ),
      );
    }).toList());
    buttons.insert(9, Container());
    buttons.add(TextButton(
      child: Icon(
        FeatherIcons.delete,
        color: colorGuide,
      ),
      style: TextButton.styleFrom(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 30),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.elliptical(65, 90)),
        ),
        animationDuration: Duration(milliseconds: 1),
        primary: widget.hitColor,
      ),
      onPressed: () {
        widget.onPinHaptic();
        setState(() {
          if (pin.isNotEmpty) pin.removeLast();
        });
      },
      onLongPress: () {
        widget.onPinHaptic();
        setState(() {
          if (pin.isNotEmpty) pin.removeRange(0, pin.length);
        });
      },
    ));
    return buttons;
  }
}

class OnboardingFeaturesPage {}

class Sketcher extends CustomPainter {
  final int total;
  final int filled;
  final double dotRadius = 5;

  Sketcher({this.total, this.filled});

  void paint(Canvas canvas, Size size) {
    final double dotSpace = 120.0 / total + 10;
    final totalOffset = ((dotRadius + dotSpace) * (total - 1) + dotRadius) / 2;
    Paint filledPaint = Paint()..color = colorBluePale;
    Paint emptyPaint = Paint()..color = Colors.grey;

    // Draw static dots
    for (int i = 0; i < total; i++) {
      canvas.drawCircle(Offset(i * (dotRadius + dotSpace) - totalOffset, 0),
          dotRadius, i < filled ? filledPaint : emptyPaint);
    }
  }

  @override
  bool shouldRepaint(Sketcher oldDelegate) {
    return oldDelegate.filled != filled;
  }
}
