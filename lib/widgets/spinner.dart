import "package:flutter/material.dart";
import 'package:dvote_common/constants/colors.dart';

class SpinnerCircular extends StatelessWidget {
  final double strokeWidth;
  SpinnerCircular({this.strokeWidth = 2});

  @override
  Widget build(context) {
    return SizedBox(
      height: iconSizeTinny,
      width: iconSizeTinny,
      child: CircularProgressIndicator(
        strokeWidth: strokeWidth,
        valueColor: AlwaysStoppedAnimation<Color>(colorGuide),
      ),
    );
  }
}
