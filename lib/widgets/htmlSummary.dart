import "package:flutter/material.dart";
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:dvote_common/constants/colors.dart';

class HtmlSummary extends StatefulWidget {
  final String htmlString;
  final bool isSecondary;
  final double maxHeight;
  final double defaultFontSize;
  final Color defaultTextColor;
  final FontWeight defaultFontWeight;
  final int characterOverflow;
  final int lineOverflow;

  HtmlSummary({this.htmlString, isSecondary = false})
      : this.isSecondary = isSecondary,
        this.maxHeight = isSecondary ? 45 : 120,
        this.defaultFontSize = isSecondary ? 15 : 17,
        this.defaultFontWeight =
            isSecondary ? fontWeightRegular : fontWeightLight,
        this.defaultTextColor = isSecondary
            ? colorDescription.withOpacity(opacitySecondaryElement)
            : colorDescription,
        this.characterOverflow = isSecondary ? 115 : 150,
        this.lineOverflow = isSecondary ? 2 : 3;

  @override
  _HtmlSummaryState createState() => _HtmlSummaryState();
}

class _HtmlSummaryState extends State<HtmlSummary>
    with TickerProviderStateMixin {
  bool collapsed = true;
  @override
  Widget build(context) {
    if ((widget.htmlString?.isEmpty ?? true) ||
        widget.htmlString.trim().isEmpty ||
        widget.htmlString.trim() == "<p></p>") return SizedBox.shrink();
    return AnimatedSize(
      alignment: Alignment.topLeft,
      curve: Curves.easeOutCubic,
      duration: Duration(milliseconds: 300),
      vsync: this,
      child: InkWell(
        onTap: () => setState(() {
          collapsed = !collapsed;
        }),
        child: Container(
          padding:
              new EdgeInsets.fromLTRB(paddingPage, 4, paddingPage, paddingPage),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: collapsed ? widget.maxHeight : double.infinity,
              minWidth: double.infinity, // Take up as much width as possible
            ),
            child: ShaderMask(
              shaderCallback: (bounds) {
                return LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [0, 1],
                  colors: <Color>[
                    Colors.transparent,
                    Colors.black,
                  ],
                  tileMode: TileMode.clamp,
                ).createShader(bounds);
              },
              blendMode: collapsed && _willOverflow()
                  ? BlendMode.dstOut
                  : BlendMode.dst,
              child: FittedBox(
                clipBehavior: Clip.antiAlias,
                fit: BoxFit.fitWidth,
                alignment: Alignment.topCenter,
                child: Html(
                  data: widget.htmlString,
                  useRichText: true,
                  defaultTextStyle: TextStyle(
                    fontSize: widget.defaultFontSize,
                    color: widget.defaultTextColor,
                    fontWeight: widget.defaultFontWeight,
                  ),
                  onLinkTap: (url) => launchUrl(url),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool _willOverflow() {
    final lineBreaks = "<li".allMatches(widget.htmlString).length +
        "<br".allMatches(widget.htmlString).length +
        "<p".allMatches(widget.htmlString).length;
    if (lineBreaks >= widget.lineOverflow) return true;
    final contentString =
        widget.htmlString.replaceAll(new RegExp(r'<.[^>]*>'), '');
    if (contentString.length >= widget.characterOverflow) return true;
    return false;
  }
}

launchUrl(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw Exception('Could not launch $url');
  }
}
