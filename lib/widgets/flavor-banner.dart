import 'package:flutter/material.dart';

class FlavorBanner extends StatelessWidget {
  final Widget child;
  final String mode;

  FlavorBanner({@required this.child, @required this.mode}) {
    if (!["dev", "beta", "production"].contains(mode))
      throw Exception("Invalid app mode");
  }

  @override
  Widget build(BuildContext context) {
    if (mode == "production") return child;

    return Stack(
      children: <Widget>[child, _buildBanner(context)],
    );
  }

  Widget _buildBanner(BuildContext context) {
    final name = mode == "beta" ? "BETA" : "DEV";
    final color = mode == "beta" ? Colors.indigo : Colors.red;

    return Container(
      width: 50,
      height: 50,
      child: CustomPaint(
        painter: BannerPainter(
            message: name,
            textDirection: Directionality.of(context),
            layoutDirection: Directionality.of(context),
            location: BannerLocation.topStart,
            color: color),
      ),
    );
  }
}
