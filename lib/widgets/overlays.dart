import "package:flutter/material.dart";
import 'package:dvote_common/widgets/loading-spinner.dart';
import 'package:dvote_common/constants/colors.dart';
import 'package:overlay_support/overlay_support.dart';

final overlayTextStyle =
    TextStyle(fontSize: fontSizeBase, fontWeight: fontWeightRegular);

/// Displays a snackbar on the screen.
OverlaySupportEntry showLoadingOverlay(String text) {
  if (text == null) throw Exception("No text");
  return showOverlay(
    (context, t) {
      return buildLoading(text);
    },
    duration: Duration(seconds: 0),
  );
}

/// Displays a snackbar on the screen.
OverlaySupportEntry showMessageOverlay(String text,
    {int duration = 4, Purpose purpose = Purpose.NONE}) {
  if (text == null) throw Exception("No text");
  return showOverlay(
    (context, t) {
      return buildMessage(text, purpose);
    },
    duration: Duration(seconds: duration),
  );
}

Widget buildLoading(String text) {
  return DefaultTextStyle(
    style: overlayTextStyle,
    child: Column(
      children: [
        Spacer(),
        Container(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              children: <Widget>[
                LoadingSpinner(color: Colors.white),
                Padding(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(text, style: overlayTextStyle),
                ),
              ],
            ),
          ),
          color: colorDescription,
        ),
      ],
    ),
  );
}

Widget buildMessage(String text, Purpose purpose) {
  return DefaultTextStyle(
    style: overlayTextStyle,
    child: Column(
      children: [
        Spacer(),
        Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 40),
            child: Text(text, style: overlayTextStyle),
          ),
          color: getColorByPurpose(purpose: purpose, isPale: true),
        ),
      ],
    ),
  );
}
